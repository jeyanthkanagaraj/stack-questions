import axios from 'axios'

export const getQuestions = (value: number) => {
  console.log(value)
  return axios.get(`https://api.stackexchange.com/2.2/questions?page=${value}&order=desc&sort=activity&site=stackoverflow`)
}
