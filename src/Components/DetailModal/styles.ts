export const Styles = () => ({
  root: {
    padding: 30,
    textAlign: 'center' as const,
    position: 'relative' as const,
    '& p': {
      marginBottom: 20,
    },
  },
  close: {
    position: 'absolute' as const,
    top: 10,
    right: 10,
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    cursor: 'pointer',
  },
  link: {
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  },
})
