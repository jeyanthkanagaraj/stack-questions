import React from 'react'
import { withStyles, Card, Typography } from '@material-ui/core'
import { Styles } from './styles'

type MyProps = {
  question: any
  classes?: any
  handleModalClose: any
}

const DetailModal: React.FC<MyProps> = ({ question, classes, handleModalClose }) => {
  return (
    <Card className={classes.root}>
      <span onClick={handleModalClose} className={classes.close}>
        X
      </span>
      <Typography>{question.title}</Typography>
      <a href={question.link} target="_blank" className={classes.link}>
        Link to the question
      </a>
    </Card>
  )
}

export default withStyles(Styles)(DetailModal)
