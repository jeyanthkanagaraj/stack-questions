import React from 'react'
import { withStyles, AppBar, Toolbar, Typography } from '@material-ui/core'
import { Styles } from './styles'

type MyProps = {
  classes: any
}

const Header: React.FC<MyProps> = ({ classes }) => {
  return (
    <AppBar position="static">
      <Toolbar className={classes.toolbar}>
        <Typography align="center" variant="h6">
          Stack Overflow Questions
        </Typography>
      </Toolbar>
    </AppBar>
  )
}

export default withStyles(Styles)(Header)
