import React, { Fragment } from 'react'
import { withStyles, Table, TableHead, TableCell, TableBody, TableRow, Paper, Dialog } from '@material-ui/core'
import moment from 'moment'
import { Styles } from './styles'
import DetailModal from '../DetailModal'

type MyProps = {
  questions: any
  classes: any
  handleModalOpen: Function
  isModalOpen: number | null
  handleModalClose: any
}

const TableDetails: React.FC<MyProps> = ({ questions, classes, isModalOpen, handleModalOpen, handleModalClose }) => {
  console.log(isModalOpen)
  return (
    <Table component={Paper} className={classes.root}>
      <TableHead className={classes.head}>
        <TableRow>
          <TableCell>Id</TableCell>
          <TableCell>Author</TableCell>
          <TableCell>Title</TableCell>
          <TableCell>Creation date</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {questions.map((question: any) => (
          <Fragment key={question.question_id}>
            <TableRow onClick={() => handleModalOpen(question.question_id)} className={classes.tableRow}>
              <TableCell>{question.question_id}</TableCell>
              <TableCell>{question.owner.display_name}</TableCell>
              <TableCell>{question.title}</TableCell>
              <TableCell>{moment(question.creation_date * 1000).format('LLL')}</TableCell>
            </TableRow>
            <Dialog open={question.question_id === isModalOpen} onClose={handleModalClose}>
              <DetailModal question={question} handleModalClose={handleModalClose} />
            </Dialog>
          </Fragment>
        ))}
      </TableBody>
    </Table>
  )
}

export default withStyles(Styles)(TableDetails)
