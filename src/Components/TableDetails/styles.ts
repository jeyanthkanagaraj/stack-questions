import { Theme } from '@material-ui/core/styles/createMuiTheme'

export const Styles = (theme: Theme) => ({
  root: {
    marginTop: 20,
    [theme.breakpoints.down('sm')]: {
      width: 360,
      overflowX: 'scroll',
      display: 'block',
    },
  },
  head: {
    backgroundColor: '#ccc' as const,
    fontWeight: 'bold' as const,
  },
  tableRow: {
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: 'rgba(0,0,0,0.1)' as const,
    },
  },
})
