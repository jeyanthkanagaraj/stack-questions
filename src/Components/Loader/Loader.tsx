import React from 'react'
import { withStyles, CircularProgress } from '@material-ui/core'
import { Styles } from './styles'

interface MyProps {
  classes: any
}

const Loader: React.FC<MyProps> = ({ classes }) => {
  return (
    <div className={classes.root}>
      <CircularProgress className={classes.loader} />
    </div>
  )
}

export default withStyles(Styles)(Loader)
