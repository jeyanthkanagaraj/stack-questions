import React, { Component } from 'react'
import { withStyles, Typography } from '@material-ui/core'
import { Styles } from './styles'
import Header from '../../Components/Header'
import { getQuestions } from '../../services'
import TableDetails from '../../Components/TableDetails'
import Loader from '../../Components/Loader'
import { uniqBy } from 'lodash'

type MyProps = {
  classes: any
}

type MyState = {
  Questions: any[]
  IsLoading: boolean
  Page: number
  PrevY: number
  ShowMessage: boolean
  IsModalOpen: number | null
}

class LandingPage extends Component<MyProps, MyState> {
  [x: string]: any
  constructor(props: any) {
    super(props)
    this.state = {
      Questions: [],
      IsLoading: false,
      Page: 1,
      PrevY: 0,
      ShowMessage: false,
      IsModalOpen: null,
    }
  }

  componentDidMount() {
    this.setState({ IsLoading: true })
    this.handleGetQuestion(this.state.Page)

    //For infinite scrolling
    var options = {
      root: null,
      rootMargin: '0px',
      threshold: 1.0,
    }

    this.observer = new IntersectionObserver(this.handleObserver, options)
    this.observer.observe(this.loadingRef)
  }

  handleGetQuestion = (currentPage: number) => {
    getQuestions(currentPage)
      .then((res) => {
        if (res.data.items.length === 0) {
          this.setState({
            ShowMessage: true,
          })
        } else {
          this.setState({ Questions: uniqBy([...this.state.Questions, ...res.data.items], 'question_id') })
          this.setState({ IsLoading: false })
        }
      })
      .catch((err) => {
        this.setState({ IsLoading: false })
        console.log(err)
      })
  }

  handleObserver = (entities: any[]) => {
    const y = entities[0].boundingClientRect.y
    if (this.state.PrevY > y) {
      const currentPage = this.state.Page + 1
      this.handleGetQuestion(currentPage)
      this.setState({ Page: currentPage })
    }
    this.setState({ PrevY: y })
  }

  handleModalOpen = (val: number) => {
    this.setState({
      IsModalOpen: val,
    })
  }

  handleModalClose = () => {
    this.setState({
      IsModalOpen: null,
    })
  }

  render() {
    const { Questions, IsLoading, ShowMessage, IsModalOpen } = this.state
    const { classes } = this.props
    return (
      <div>
        {IsLoading && <Loader />}
        <Header />
        <TableDetails
          questions={Questions}
          isModalOpen={IsModalOpen}
          handleModalOpen={this.handleModalOpen}
          handleModalClose={this.handleModalClose}
        />
        <div ref={(loadingRef) => (this.loadingRef = loadingRef)} className={classes.loading}>
          <Typography align="center">Loading...</Typography>
        </div>
        {ShowMessage && <Typography className={classes.end}>The End</Typography>}
      </div>
    )
  }
}

export default withStyles(Styles)(LandingPage)
