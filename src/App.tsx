import React from 'react';
import LandingPage from './view/LandingPage'
import './App.css';

function App() {
  return <LandingPage />
}

export default App;
