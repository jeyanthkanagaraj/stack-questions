# Stack Overflow Questions

Application to display stackoverflow questions.

## Tech Stack

REactJS, TypeScript

## Prerequisite

- Node v10+
- NPM v6+

## Quick Start

To get start, Please follow the below steps

```
git clone https://gitlab.com/jeyanthkanagaraj/stack-questions.git
cd stack-questions
npm install or yarn install
npm start or yarn add

```

The application will start compliling, once it compiled successfully, it will automatically open your browser and the application will run in http://localhost:3000/
